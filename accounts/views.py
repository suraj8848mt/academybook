from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import DetailView
from django.views import View

from Academy_Book.language import Translate
# from .signals import user_logged_in
from django.contrib.auth import get_user_model, login, logout
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import EmailMessage
from celery import shared_task
from time import sleep
import stripe
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string

from .signals import user_logged_in
from .token_generator import account_activation_token
from .models import Profile, Customer
from .tasks import sleepy
from .forms import UserCreationForm, UserLoginForm, UserUpdateForm

User = get_user_model()

stripe.api_key = 'sk_test_1YjooVfuUWXkzdTeUrLtJHTe00lxb7H6Tk'


def home(request):
    return render(request, "home.html", {})


def join(request):
    return render(request, 'plans/join.html')


@login_required
def checkout(request):
    try:
        if request.user.customer.membership:
            return redirect('posts:post-list')
    except Customer.DoesNotExist:
        pass
    coupons = {'halloween': 31, 'welcome': 10}
    if request.method == 'POST':
        stripe_customer = stripe.Customer.create(email=request.user.email, source=request.POST['stripeToken'])
        plan = 'plan_HCgG3B8clQv5cl'
        if request.POST['plan'] == 'yearly':
            plan = 'plan_HCgGkYAOPMRYAI'
        if request.POST['coupon'] in coupons:
            percentage = coupons[request.POST['coupon'].lower()]
            try:
                coupon = stripe.Coupon.create(duration='once', id=request.POST['coupon'].lower(),
                                              percentage_off=percentage)
            except:
                pass
            subscription = stripe.Subscription.create(customer=stripe_customer.id, items=[{'plan': plan}],
                                                      coupon=request.POST['coupon'].lower())
        else:
            subscription = stripe.Subscription.create(customer=stripe_customer.id, items=[{'plan': plan}])
        customer = Customer()
        customer.user = request.user
        customer.stripe_id = stripe_customer.id
        customer.membership = True
        customer.cancel_at_period_end = False
        customer.stripe_subscription_id = subscription.id
        customer.save()
        return redirect('posts:post-list')
    else:
        plan = 'monthly'
        coupon = 'none'
        price = 1000
        og_dollar = 1000
        coupon_dollar = 0
        final_dollar = 1000
        if request.method == 'GET' and 'plan' in request.GET:
            if request.GET['plan'] == 'yearly':
                plan = 'yearly'
                price = 10000
                og_dollar = 10000
                final_dollar = 10000

        if request.method == 'GET' and 'coupon' in request.GET:
            if request.GET['coupon'].lower() in coupons:
                coupon = request.GET['coupon'].lower()
                percentage = coupons[coupon]
                coupon_price = int((percentage / 100) * price)
                price = price - coupon_price
                coupon_dollar = str(coupon_price) + '.' + str(coupon_price)[-2:]
                final_dollar = str(price) + '.' + str(price)[-2:]

    return render(request, 'plans/checkout.html', {
        'plan': plan, 'coupon': coupon,
        'price': price, 'og_dollar': og_dollar, 'final_dollar': final_dollar,
        'coupon_dollar': coupon_dollar})


def settings(request):
    return render(request, 'plans/settings.html')


@shared_task()
def register(request, *args, **kwargs):
    form = UserCreationForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        current_site = get_current_site(request)
        email_subject = 'Activate your account'
        message = render_to_string('activate_account.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)).encode().decode(),
            'token': account_activation_token.make_token(user),
        })
        to_email = form.cleaned_data.get('email')
        email = EmailMessage(email_subject, message, to=[to_email])
        sleep(10)
        email.send()

        return HttpResponse('We have send You an email, please confirm your email address to complete registration')
    else:
        form = UserCreationForm()
    context = {
        "form": form
    }
    return render(request, 'accounts/register.html', context)


def activate_account(request, uidb64, token, backend='django.contrib.auth.backends.ModelBackend'):
    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user, backend)
        return HttpResponse('Your account has been activate successfully')
    else:
        return HttpResponse('Activation link is invalid!')


def login_view(request, backend='django.contrib.auth.backends.ModelBackend', user=None, *args, **kwargs, ):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        user_obj = form.cleaned_data.get('user_obj')
        login(request, user_obj, backend)
        user_logged_in.send(user_obj.__class__, instance=user_obj, request=request)
        return HttpResponseRedirect('/')

    context = {
        "form": form
    }
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/accounts/login/')


class UserDetailView(LoginRequiredMixin, Translate, DetailView):
    queryset = User.objects.all()
    template_name = "accounts/myuser_detail.html"

    def get_object(self, queryset=None):
        return get_object_or_404(User, username__iexact=self.kwargs.get('username'))

    def get_context_data(self, *args, **kwargs):
        context = super(UserDetailView, self).get_context_data(*args, **kwargs)
        context['following'] = Profile.objects.is_following(self.request.user, self.get_object())
        return context


class UserFollow(View):
    def get(self, request, username, *args, **kwargs):
        toggleUser = get_object_or_404(User, username__iexact=username)
        if request.user.is_authenticated:
            is_following = Profile.objects.toggle_follow(request.user, toggleUser)
        return redirect("profiles:user-detail", username=username)
