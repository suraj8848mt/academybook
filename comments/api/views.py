from django.db.models import Q
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated, AllowAny
from posts.api.pagination import StandardResultsPagination

from comments.models import Comment
from .serializers import (CommentSerializer,
                          create_comment_serializer,
                          CommentDetailSerializer,
                          CommentEditSerializer, CommentListSerializer)


class EditCommentAPIView(generics.RetrieveAPIView):
    queryset = Comment.objects.filter(id__gte=0)
    serializer_class = CommentEditSerializer
    lookup_field = 'pk'


class CommentListAPIView(generics.ListAPIView):
    serializer_class = CommentListSerializer
    permission_classes = [AllowAny]
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['content', 'user__first_name']
    pagination_class = StandardResultsPagination

    def get_queryset(self, *args, **kwargs):
        queryset_list = Comment.objects.all()
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(
                Q(content__icontains=query) |
                Q(user__first_name_icontains=query) |
                Q(user__last_name_icontains=query)

            ).distinct()
        return queryset_list


class CreateCommentAPIView(generics.CreateAPIView):
    queryset = Comment.objects.all()
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        model_type = self.request.GET.get("type")
        slug = self.request.GET.get('slug')
        parent_id = self.request.GET.get("parent_id", None)
        return create_comment_serializer(model_type=model_type, slug=slug, parent_id=parent_id, user=self.request.user)


class CommentDetailAPIView(DestroyModelMixin, UpdateModelMixin, generics.RetrieveAPIView):
    queryset = Comment.objects.filter(id__gte=0)
    serializer_class = CommentDetailSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

