from django.contrib.contenttypes.models import ContentType
from rest_framework import generics
from django.shortcuts import get_object_or_404, redirect, render
from urllib.parse import quote_plus
from django.utils import translation
from rest_framework.permissions import IsAuthenticated

from analytics.mixins import ObjectViewedMixin
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q
from .pagination import StandardResultsPagination

from posts.models import Post
from .serializers import PostSerializer
from ..mixins import Paid


class LikedToggleAPIView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk=None, format=None):
        post_qs = Post.objects.filter(pk=pk)
        message = "Not Allowed"
        if request.user.is_authenticated:
            is_liked = Post.objects.like_toggle(request.user, post_qs.first())
            return Response({'liked': is_liked})
        return Response({"message": message}, status=400)


class RePostApiView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk=None, format=None):
        post_qs = Post.objects.filter(pk=pk)
        message = "Not Allowed"
        if post_qs.exists() and post_qs.count() == 1:
            new_post = Post.objects.repost(request.user, post_qs.first())
            if new_post is not None:
                data = PostSerializer(new_post).data
                return Response(data)
            message = "Cannot post "
        return Response({"message": message}, status=400)


class PostCreateApiView(generics.CreateAPIView):
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class SearchListApiView(generics.ListAPIView):
    queryset = Post.objects.all().order_by("-timestamp")
    serializer_class = PostSerializer
    pagination_class = StandardResultsPagination

    def get_serializer_context(self, *args, **kwargs):
        context = super(SearchListApiView, self).get_serializer_context(*args, **kwargs)
        context['request'] = self.request
        return context

    def get_queryset(self, *args, **kwargs):
        qs = self.queryset
        query = self.request.GET.get("q", None)
        if query is not None:
            qs = qs.filter(
                Q(content__icontains=query) |
                Q(user__username__icontains=query)
            )
        return qs


class PostListApiView(Paid, generics.ListAPIView):
    serializer_class = PostSerializer
    permission_class = (IsAuthenticated, )
    pagination_class = StandardResultsPagination

    def get_serializer_context(self, *args, **kwargs):
        context = super(PostListApiView, self).get_serializer_context(*args, **kwargs)
        context['request'] = self.request
        return context

    def get_queryset(self, *args, **kwargs):
        requested_user = self.kwargs.get("username")
        if requested_user:
            qs = Post.objects.filter(user__username=self.request.user).order_by("-timestamp")
        else:
            i_am_following = self.request.user.profile.get_following()
            qs1 = Post.objects.filter(user__in=i_am_following)
            qs2 = Post.objects.filter(user=self.request.user)
            qs = (qs1 | qs2).distinct().order_by("-timestamp")
        query = self.request.GET.get("q", None)
        if query is not None:
            qs = qs.filter(
                Q(content__icontains=query) |
                Q(user__username__icontains=query)
            )
        return qs


class PostDetailAPIView(ObjectViewedMixin, generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.AllowAny]
    pagination_class = StandardResultsPagination

    def get_queryset(self, *args, **kwargs):
        post_id = self.kwargs.get('pk')
        instance = Post.objects.filter(pk=post_id)

        if instance.exists() and instance.count() == 1:
            parent_obj = instance.first()
            qs1 = parent_obj.get_children()
            instance = (instance | qs1).distinct().extra(select={"parent_id_null": "parent_id IS NULL"})
        # object_viewed_signal.send(qs.__class__, instance=qs, request=request)
        return instance.order_by("-parent_id_null", "timestamp")
