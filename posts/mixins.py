from django import forms
from django.forms.utils import ErrorList
from django.shortcuts import render, get_object_or_404, redirect

from posts.models import Post


class FormUserNeededMixin(object):
    def form_valid(self, form):
        if self.request.user.is_authenticated:
            form.instance.user = self.request.user
            return super(FormUserNeededMixin, self).form_valid(form)
        else:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList(['User must be logged in to continue'])
            return self.form_invalid(form)


class UserOwnerMixin(object):
    def form_valid(self, form):
        if form.instance.user == self.request.user:
            return super(UserOwnerMixin, self).form_valid(form)
        else:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList(['This User is not allowed to change this data'])
            return self.form_invalid(form)


class Paid(object):
    def form_valid(self, form):
        if form.instance.user == self.request.user.is_paid:
            return super(Paid, self).form_valid(form)
        else:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList(['This User is not allowed to change this data'])
            return self.form_invalid(form)


